package model;

public class Util {

	public static byte[] int2bytes(int num) {
		byte[] buffer = new byte[4];
		
		for (int i = 0; i < 4; i++) {
			buffer[i] = (byte) ((num >> 8 * i) & 0xff);
		}
		
		return buffer;
	}
	
	public static int bytes2int(byte[] buffer, int offset) {
		int num = 0;
		for (int i = 0; i < 4; i++) {
			num += ((int)(buffer[i + offset] & 0xff) << 8 * i);
		}
		
		return num;
	}
}
