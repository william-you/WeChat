package model;

public class Test {

	public static void main(String[] args) {
		LogonPacket packet = new LogonPacket();
		
		packet.userName = "william";
		packet.password = "123456";
		
		// Client
		byte[] buffer = packet.serialize();
		
		
		// Server
		LogonPacket receivedPacket = new LogonPacket();
		receivedPacket.unserialize(buffer);
		System.out.println(receivedPacket.userName + ", " + receivedPacket.password);

	}

}
