package com.example.lenovo.wechat;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;



public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private static final String TAG = "ChatAdapter";

    private List<Chat> chatList;

    private Activity mActivity;


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ImageView imageView = itemView.findViewById(R.id.chat_imageView);
            TextView textView = itemView.findViewById(R.id.chat_textView);
            linearLayout = itemView.findViewById(R.id.chat_linearLayout);
        }
    }

    public ChatAdapter(Activity activity, List<Chat> chatList) {
        mActivity = activity;
        this.chatList = chatList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int i) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startActivity(new Intent(mActivity, ChatActivity.class));
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ImageView imageView = viewHolder.linearLayout.findViewById(R.id.chat_imageView);
        TextView textView = viewHolder.linearLayout.findViewById(R.id.chat_textView);
        imageView.setImageResource(chatList.get(i).getImageId());
        textView.setText(chatList.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }
}
