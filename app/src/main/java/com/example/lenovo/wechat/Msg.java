package com.example.lenovo.wechat;

public class Msg {
    private Type type;
    private String content;

    public Msg(String content, Type type) {
        this.type = type;
        this.content = content;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public enum Type {
        RECEIVED, SENT;
    }
}