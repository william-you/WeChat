package com.example.lenovo.wechat;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter {
    public List<Contact> data;


    public ContactAdapter(List<Contact> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contact_item, viewGroup, false);
        return new RecyclerView.ViewHolder(view){};
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ImageView ivAvatar = viewHolder.itemView.findViewById(R.id.iv_contact_avatar);
        TextView tvName = viewHolder.itemView.findViewById(R.id.tv_contact_name);
        ivAvatar.setImageResource(data.get(i).avatar);
        tvName.setText(data.get(i).name);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
