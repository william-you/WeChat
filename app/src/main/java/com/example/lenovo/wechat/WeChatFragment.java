package com.example.lenovo.wechat;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WeChatFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WeChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WeChatFragment extends Fragment {

    private List<Chat> chatList = new ArrayList<>();

    private ViewGroup mContainer;

    private Button mBtnSend;
    private EditText mEdMessage;

    public WeChatFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_we_chat, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.chat_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        chatList.add(new Chat(R.mipmap.rectangle_portrait, "聊天"));
        chatList.add(new Chat(R.mipmap.rectangle_portrait, "聊天"));
        chatList.add(new Chat(R.mipmap.rectangle_portrait, "聊天"));

        ChatAdapter adapter = new ChatAdapter(getActivity(), chatList);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        mBtnSend = mContainer.findViewById(R.id.btn_send);
    }
}
