package com.example.lenovo.wechat;

public class Contact {
    public int avatar;
    public String name;
    public String phone;

    public Contact(int avatar, String name, String phone) {
       this.avatar = avatar;
       this.name = name;
       this.phone = phone;
    }
}
