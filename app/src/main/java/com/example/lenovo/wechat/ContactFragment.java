package com.example.lenovo.wechat;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment {
    public List<Contact> mContactList;

    public ContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView contactList = getActivity().findViewById(R.id.rlv_contact_list);
        contactList.setLayoutManager(new LinearLayoutManager(getContext()));

        mContactList = new ArrayList<>();
        mContactList.add(new Contact(R.mipmap.rectangle_portrait, "william", "12345678901"));
        mContactList.add(new Contact(R.drawable.head1, "多啦A梦", "12345678901"));
        mContactList.add(new Contact(R.drawable.head2, "灰太狼", "12345678901"));

        ContactAdapter adapter = new ContactAdapter(mContactList);
        contactList.setAdapter(adapter);

    }
}
