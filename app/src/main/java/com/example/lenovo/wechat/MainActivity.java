package com.example.lenovo.wechat;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton mBtnWeChat;
    private ImageButton mBtnContact;
    private ImageButton mBtnFind;
    private ImageButton mBtnMe;

    private WeChatFragment mWeChatFragment = null;
    private ContactFragment mContactFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_new);

        mBtnWeChat = findViewById(R.id.btn_wechat);
        mBtnContact = findViewById(R.id.btn_contact);
        mBtnFind = findViewById(R.id.btn_find);
        mBtnMe = findViewById(R.id.btn_me);

        mBtnWeChat.setOnClickListener(this);
        mBtnContact.setOnClickListener(this);
        mBtnFind.setOnClickListener(this);
        mBtnMe.setOnClickListener(this);

        selectTab(0);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_wechat:
                selectTab(0);
                break;
            case R.id.btn_contact:
                selectTab(1);
                break;
            case R.id.btn_find:
                selectTab(2);
                break;
            case R.id.btn_me:
                selectTab(3);
                break;
             default:
                 Log.e("MainActivity", "on click error!");
        }
    }

    private void selectTab(int index) {
        setTabButtonDefaultColor();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        hideFragments(transaction);

        switch (index) {
            case 0:
                mBtnWeChat.setImageResource(R.mipmap.tab_weixin_pressed);
                if (mWeChatFragment == null) {
                    mWeChatFragment = new WeChatFragment();
                    transaction.add(R.id.fl_content, mWeChatFragment);
                } else {
                    transaction.show(mWeChatFragment);
                }
                break;
            case 1:
                mBtnContact.setImageResource(R.mipmap.tab_address_pressed);
                if (mContactFragment == null) {
                    mContactFragment = new ContactFragment();
                    transaction.add(R.id.fl_content, mContactFragment);
                } else {
                    transaction.show(mContactFragment);
                }
                break;
            case 2:
                mBtnFind.setImageResource(R.mipmap.tab_find_frd_pressed);
                break;
            case 3:
                mBtnMe.setImageResource(R.mipmap.tab_settings_pressed);
                break;
        }
        transaction.commit();
    }

    private void hideFragments(FragmentTransaction transaction) {
        if (mWeChatFragment != null)
        {
            transaction.hide(mWeChatFragment);
        }

        if (mContactFragment != null) {
            transaction.hide(mContactFragment);
        }
    }

    private void setTabButtonDefaultColor() {
        mBtnWeChat.setImageResource(R.mipmap.tab_weixin_normal);
        mBtnContact.setImageResource(R.mipmap.tab_address_normal);
        mBtnFind.setImageResource(R.mipmap.tab_find_frd_normal);
        mBtnMe.setImageResource(R.mipmap.tab_settings_normal);
    }
}
